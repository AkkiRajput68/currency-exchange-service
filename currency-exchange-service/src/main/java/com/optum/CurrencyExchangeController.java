package com.optum;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrencyExchangeController {

	private Logger logger=LoggerFactory.getLogger(CurrencyExchangeController.class);
	
	@Autowired
	private Environment environment;
	
	@RequestMapping(value="/currency-exchange/from/{from}/to/{to}",method = RequestMethod.GET)
	public CurrencyExchange retrieveExchangeValue(@PathVariable String from,@PathVariable String to) {
		
		logger.info("retrieveExchangeValue called with {} to {}",from,to);
		
		CurrencyExchange ce=new CurrencyExchange();
		
		ce.setId(1);
		ce.setFrom(from);
		ce.setTo(to);
		ce.setConversionMultiple(BigDecimal.valueOf(73));
		
		//String host=environment.getProperty("HOSTNAME");
		String host="";
		ce.setEnvironment(host);
		
		return ce;
		
		
	}
}
